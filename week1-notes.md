# Week 1:  Time Value of Money

Money has a time unit associated with it, therefore we can't simply add cash flows occurring at different times.

## Compounding and Discounting

We can move cash flows forward or backward in time via the following formula

$$
CF_{from,to} = CF_{from} \cdot (1 + i)^{to - from}
$$

Where $to$ is the time period we want as our baseline for comparison/aggregation and $from$ is the time period for the cash flow we want to convert to the baseline.

For example, if we want to get the value of $CF_{5}$ at period $0$ we do

$$
CF_{5,0} = CF_{5} \cdot (1 + i)^{0 - 5} = CF_{5} \cdot (1 + i)^{-5}
$$

This is an example of "discounting", where we discount the *future* cash flow to some period in the past.  Period 0 is the present period and therefore when we bring a cash flow to the present, it's now as the "Present Value".  

To bring a cash flow forward in time, we use the same formula, though this time $to > from$ so the result will be greater than the original (as long as $i > 0$).

$$
CF_{0,5} = CF_{5} \cdot (1 + i)^{5 - 0} = CF_{5} \cdot (1 + i)^{5}
$$

In this case, we're *compounding* the interest.  When we bring a cash flow forward like this, we get it's "Future Value".

## Annuity

The annuity formula


## Relationship between $PV$ and the discount (interest) rate $i$

Graph depicting PV as a function of the interest rate with fixed cash flow (of 1) and compounding periods (30).

The y-axis represents the present value of the sum of the cash flows over all
periods.  So, if the interest rate were 0, this value would be 30.  And the
higher the interest rate, the lower the PV.  

<svg width="500" height="500"></svg>
<script src="https://d3js.org/d3.v4.min.js"></script>
<script>

const COMPOUNDING_PERIODS = 30;
const CASH_FLOW_AMOUNT = 1;

var svg = d3.select("svg"),
    margin = {top: 20, right: 20, bottom: 30, left: 50},
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom,
    g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var pv = function(i) {
    // compute pv discount factor as function of interest rate
    n = COMPOUNDING_PERIODS;
    c = CASH_FLOW_AMOUNT;
    return c*((1 - Math.pow((1 + i), -n)*(1 + i)) / i);
}

var dataset = d3.range(0.01, 0.25, 0.01).map(
    function(i) {
      return { interest_rate: i, pv: pv(i) }; 
    });

var x = d3.scaleLinear()
    .rangeRound([0, width])
    .domain(d3.extent(dataset, function(d) { return d.interest_rate; }));

var y = d3.scaleLinear()
    .rangeRound([height, 0])
    .domain(d3.extent(dataset, function(d) { return d.pv; }));

var line = d3.line()
    .x(function(d) { return x(d.interest_rate); })
    .y(function(d) { return y(d.pv); });

g.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x))
  .append("text")
    .attr("fill", "#000")
    .attr("transform", "translate(" + (+width/2) + ",30)")
    .attr("text-anchor", "middle")
    .text("Interest Rate");

g.append("g")
    .call(d3.axisLeft(y))
  .append("text")
    .attr("fill", "#000")
    .attr("transform", "translate(-30," + (+height/2) + ")rotate(-90)")
    .attr("text-anchor", "middle")
    .text("PV");

g.append("path")
    .datum(dataset)
    .attr("fill", "none")
    .attr("stroke", "steelblue")
    .attr("stroke-linejoin", "round")
    .attr("stroke-linecap", "round")
    .attr("stroke-width", 1.5)
    .attr("d", line);

</script>

# Wharton Corporate Finance Notes

My notes on Corporate Finance from Wharton on Coursera

## Posting

Notes are taken in Markdown and converted to html via [pandoc](pandoc.org)

Math notation is [katex](https://khan.github.io/KaTeX/) flavored and generated

Hosting is via gitlab-pages (see `.gitlab-ci.yml` for config)

All static site files are kept under `public` dir, which gitlab-pages references for hosting at `cj0n.gitlab.io/<project-name>`

Once notes are taken in a markdown file, do the following:

```
pandoc -s --css github-pandoc.css -o public/<notes_file>.html <notes_file>.md
```

Notes:  
- `-s` is for "standalone document" meaning that the output html will include references to css or anything else required

- `github-pandoc.css` is copied into public dir on project creation

For markdown that requires katex support, use the following

```
pandoc -s --css github-pandoc.css --katex -t html5 -o public/<notes_file_with_katex>.html <notes_file_katex>.md
```

Note:  `-t html5` is needed for including references to katex javascript libraries

On pushing to remote origin/master, gitlab will publish pages within a few minutes
